open Util

let attacker_wins f =
  Random.float 1.0 < f

let attacker_wins_experiment ~params f n =
  let time_attacker = ref 0 in
  let time_honest = ref 0 in
  for i = 0 to n-1 do
    let prio_attacker = ref 0 in
    let prio_honest = ref 0 in
    let e_attacker = ref 0 in
    let e_honest = ref 0 in

    (* pick a random prio *)
    (* if attacker wins, attacker has prio 0 and the prio of honest is the one where attacker doesn't win *)
    if attacker_wins f then
      begin
        prio_attacker := 0;
        prio_honest := 1;
        while attacker_wins f do
          prio_honest := !prio_honest + 1;
        done;
      end

    (* similarly if honest wins *)
    else
      begin
        prio_honest := 0;
        prio_attacker := 1;
        while not (attacker_wins f) do
          prio_attacker := !prio_attacker + 1;
        done;
      end;

    (* pick a random e *)
    for e = 0 to 32 do
      if attacker_wins f then
        e_attacker := !e_attacker + 1
      else
        e_honest := !e_honest + 1
    done;
    let e_attacker = if i = 0 then 32 else !e_attacker in
    time_attacker := !time_attacker + delay ~params !prio_attacker e_attacker;
    time_honest := !time_honest + delay ~params !prio_honest !e_honest;
  done;

  (* attacker is faster *)
  !time_attacker < !time_honest


let simulation ~params f n num_experiments =
  let c = ref 0 in
  for i = 0 to num_experiments do
    if attacker_wins_experiment ~params f n then
      c := !c + 1
  done;
  (float_of_int !c) /. (float_of_int num_experiments)
