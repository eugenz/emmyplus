open Util
open Simulations
open Selfish_baking
open Forks

let precision = 1e-6

let simulation_experiment () =
  let num_experiments = int_of_string Sys.argv.(1) in
  let num_blocks = int_of_string Sys.argv.(2) in
  (* let blks_choices = List.init 15 (fun x -> x + 1) in *)
  let blks_choices = [num_blocks] in
  let init_endo_choices = [18] in
  let dp_choices = [40] in
  let stake_choices = [0.1; 0.15; 0.2; 0.25; 0.30; 0.35; 0.4; 0.45; 0.5; 0.6; 0.7; 0.8; 0.9] in
  Printf.printf "init_endo,dp,fork_depth,stake,proba\n";
  iter4 (fun ie dp nblks f ->
      let params = { init_endo = ie; delta_prio = dp; delta_endo = 8;
                     total_endo = 32; typical_endo = 31 } in
      let pp = simulation ~params f nblks num_experiments in
      Printf.printf "%d, %d, %d, %.02f, %g\n" ie dp nblks f pp
    )
    init_endo_choices dp_choices blks_choices stake_choices

let seen_blocks_experiment () =
  let num_samples = int_of_string Sys.argv.(1) in

  let total_endo = 32 in
  let typical_endo = 31 in
  let delta_endo = 1 in

  let seen_blks_choices = List.init 10 (fun x -> x + 1) in
  let seen_delays_ratio_choices = [(0, 0); (2, 6); (2, 0); (0, 6)] in
  let epsilon_choices = [10. ** (-12.)] in
  let init_endo_choices = [18; 24] in
  let dp_choices = [5; 8] in
  let stake_choices = [0.1; 0.2; 0.3; 0.4; 0.5] in

  Printf.printf "init_endo,dp,epsilon,stake,seen_blocks,seen_delay_ratio,proba,error\n";
  iter6 (fun ie dp epsilon f seen_blks seen_delay_ratio ->
      let params = { init_endo = ie; delta_prio = dp; delta_endo;
                     total_endo; typical_endo } in
      let _blks, pp, tot = proba_seen_blocks ~params num_samples f seen_blks seen_delay_ratio epsilon in
      Printf.printf "%d, %d, %g, %.02f, %d, %d, %g, %g\n"
        ie dp epsilon f seen_blks seen_delay_ratio pp (1. -. tot)
    )
    init_endo_choices dp_choices epsilon_choices stake_choices seen_blks_choices seen_delays_ratio_choices


let default_params = {
   total_endo = 32;
   typical_endo = 31;
   init_endo = 24;
   delta_prio = 40;
   delta_endo = 8;
}

let main_all () =
  if Array.length Sys.argv < 2 then
    print_string "[NOT UP TO DATE, read the code] Usage: forks <num_samples> fork <stake> <depth>
   or: forks <num_samples> seen <stake> <seen_blocks> <seen_delay> <depth>\n"
  else
    match Sys.argv.(1) with

    | "scenario" ->
      let f_proba = match Sys.argv.(2) with
        | "(1)" -> proba_1block
        | "(2)" -> proba_2blocks
        | "(3)" -> proba_3blocks
        | _ -> failwith "unknown scenario: use '(1)', '(2)', '(3)'"
      in
      let stake = float_of_string Sys.argv.(3) in
      let pp = f_proba ~params:default_params stake in
      Printf.printf "\nThe probability that an adversary that has fraction %.2f of the stake steals a block at level L+1 in scenario %s is %g.\n" stake Sys.argv.(2) pp

    | "scenario_tikz_curve" ->
      let f_proba = match Sys.argv.(2) with
        | "(1)" -> proba_1block ~params:default_params
        | "(2)" -> proba_2blocks ~params:default_params
        | "(3)" -> proba_3blocks ~params:default_params
        | "(sum)" -> fun stake ->
          proba_1block ~params:default_params stake +.
          proba_2blocks ~params:default_params stake +.
          proba_3blocks ~params:default_params stake
        | _ -> failwith "unknown scenario: use '(1)', '(2)', '(3)'"
      in
      curve f_proba

    | "scenario_rewards" ->
      let f_proba = match Sys.argv.(2) with
        | "(1r)" -> proba_1block_with_rewards
        | "(2r)" -> proba_2blocks_with_rewards
        | _ -> failwith "unknown scenario: use '(1)' or '(2)'"
      in
      let stake = float_of_string Sys.argv.(3) in
      let pp, total_pp, reward_honest_chain, reward_dishonest_chain =
        f_proba ~params:default_params stake in
      Printf.printf "The probability that an adversary that has fraction %.2f of the stake \
                     steals a block at level L+1 is %g (total_pp = %g).\n" stake pp total_pp;
      (* Printf.printf "When the attacker succeeds, its expected reward is %f.\n" reward *)
      Printf.printf "The expected reward on the honest chain is %g.\n\
                     The expected reward on the dishonest chain is %g.\n"
        reward_honest_chain reward_dishonest_chain

    | "scenario_rewards_tikz_curve" ->
      let f_proba = match Sys.argv.(2) with
        | "(1)" -> proba_1block_with_rewards
        | "(2)" -> proba_2blocks_with_rewards
        | _ -> failwith "unknown scenario: use '(1)' or '(2)'"
      in
      curve (fun stake ->
          let pp, _total, _reward_honest_chain, _reward_dishonest_chain =
            f_proba ~params:default_params stake in
          pp)

    | "scenario_rewards_csv" ->
      let params = default_params in
      let f_proba = match Sys.argv.(2) with
        | "(1)" -> proba_1block_with_rewards ~params ~reward_policy:EmmyPlus
        | "(2)" -> proba_2blocks_with_rewards ~params ~reward_policy:EmmyPlus
        | "(1emmy)" -> proba_1block_with_rewards ~params ~reward_policy:Emmy
        | "(2emmy)" -> proba_2blocks_with_rewards ~params ~reward_policy:Emmy
        | _ -> failwith "unknown scenario: use '(1)' or '(2)' or '(1emmy)' or '(2emmy)'"
      in
      List.iter (fun stake ->
          let pp, pp_total, reward_honest_chain, reward_dishonest_chain = f_proba stake in
          Printf.printf "%.2f, %g, %g, %g, %g\n"
            stake pp pp_total reward_honest_chain reward_dishonest_chain)
        (List.init 50 (fun i -> 0.1 +. (float_of_int i) /. 100.))

    | "analysis_rewards_csv" ->
      let params = default_params in
      List.iter (fun stake ->
          let _, pp_t1, rew_hc1, rew_dc1 = proba_1block_with_rewards ~params ~reward_policy:EmmyPlus stake in
          let _, pp_t2, rew_hc2, rew_dc2 = proba_2blocks_with_rewards ~params ~reward_policy:EmmyPlus stake in

          let _, pp_t1e, rew_hc1e, rew_dc1e = proba_1block_with_rewards ~params ~reward_policy:Emmy stake in
          let _, pp_t2e, rew_hc2e, rew_dc2e = proba_2blocks_with_rewards ~params ~reward_policy:Emmy stake in

          (* we print:
             - stake
             - expected rewards on honest chain, for Emmy+ policy
             - expected rewards on dishonest chain, for Emmy+ policy
             - expected rewards on honest chain, for Emmy policy
             - expected rewards on dishonest chain, for Emmy policy
          *)
          Printf.printf "%.2f, %g, %g, %g, %g\n"
            stake
            (pp_t1 *. rew_hc1 +. pp_t2 *. rew_hc2)
            (pp_t1 *. rew_dc1 +. pp_t2 *. rew_dc2)
            (pp_t1e *. rew_hc1e +. pp_t2e *. rew_hc2e)
            (pp_t1e *. rew_dc1e +. pp_t2e *. rew_dc2e);
        )
        (List.init 50 (fun i -> 0.1 +. (float_of_int i) /. 100.))

    | "simulation" ->
      simulation_experiment ()

    | "fork" ->
      let num_samples = int_of_string Sys.argv.(2) in
      let stake = float_of_string Sys.argv.(3) in
      let depth = int_of_string Sys.argv.(4) in
      (* let res = explore stake depth in
          * Printf.printf "%.2f %.6f\n" stake res; *)
      let wins, total = proba_fork_of_first_length ~can_have_best_prio_at_first_block:true
          ~params:default_params
          num_samples depth stake in
      Printf.printf "\nThe probability that an adversary that has fraction %.2f of the stake bakes a fork of length %i blocks that is faster than the main chain is about %g\n" stake depth wins;
      Printf.printf "We assume that the honest bakers bake as fast as they can in the future.\n";
      Printf.printf "(Estimate computed with %i samples. Total probability: %g -- should be 1 if the precision is satisfactory.)\n" num_samples total

    | "proba_fork_length" ->
      let num_samples = int_of_string Sys.argv.(2) in
      let epsilon = float_of_string Sys.argv.(3) in
      (* let stake_choices = List.init 28 (fun i -> float_of_int (20 + i) *. 0.01) in *)
      let stake_choices = [ 0.2; 0.3; 0.4; 0.45 ] in
      List.iter (fun stake ->
          let depth = ref 1 in
          let pp = ref (2. *. epsilon) in
          while !pp > epsilon do
            let proba_fork, total_proba = proba_fork_of_first_length ~params:default_params
                ~can_have_best_prio_at_first_block:true num_samples !depth stake in
            if 1. -. total_proba > precision then
              Printf.eprintf "Warning: total probability not 1 (but %g) for stake %.2f and depth %d\n"
                total_proba stake !depth;
            Printf.printf "%.2f, %d, %g\n%!" stake !depth proba_fork;
            pp := proba_fork;
            incr depth
          done)
        stake_choices

    | "fork_length" ->
      let num_samples = int_of_string Sys.argv.(2) in
      let epsilon = float_of_string Sys.argv.(3) in
      let stake_choices = List.init 20 (fun i -> float_of_int (20 + i) *. 0.01) in
      List.iter (fun stake ->
          let depth = ref 1 in
          let pp = ref (2. *. epsilon) in
          while !pp > epsilon do
            let proba_fork, total_proba = proba_fork_of_first_length ~params:default_params
                ~can_have_best_prio_at_first_block:true num_samples !depth stake in
            if 1. -. total_proba > precision then
              Printf.eprintf "Warning: total probability not 1 (but %g) for stake %.2f and depth %d\n"
                total_proba stake !depth;
            pp := proba_fork;
            incr depth
          done;
          Printf.printf "%.2f, %d\n%!" stake !depth;
        )
        stake_choices

    | "forks_tikz_curve" ->
      let num_samples = int_of_string Sys.argv.(2) in
      let epsilon = float_of_string Sys.argv.(3) in
      curve (fun stake ->
          let _nb_iter, pp_wins, _pp_total = proba_fork ~can_have_best_prio_at_first_block:true
              ~params:default_params
              num_samples epsilon stake in
          pp_wins
        )

    | "selfish_baking" ->
      let num_samples = int_of_string Sys.argv.(2) in
      let epsilon = float_of_string Sys.argv.(3) in
      let stake_choices = List.init 31 (fun i -> float_of_int (20 + i) *. 0.01) in
      List.iter (fun stake ->
          let _nb_iter, pp, _pp_total = proba_fork ~can_have_best_prio_at_first_block:false
              ~params:default_params
              num_samples epsilon stake in
          Printf.printf "%.2f, %g\n%!" stake pp;
        ) stake_choices

    | "selfish_baking_max_depth_3" ->
      let num_samples = int_of_string Sys.argv.(2) in
      let k = 3 in
      let stake_choices = List.init 26 (fun i -> float_of_int (20 + i) *. 0.01) in
      List.iter (fun stake ->
          let sum_pp = ref 0. in
          for i = 1 to k do
            let pp, _pp_total = proba_fork_of_first_length ~can_have_best_prio_at_first_block:false
                ~params:default_params
                num_samples i stake in
            sum_pp := !sum_pp +. pp;
          done;
          Printf.printf "%.2f, %g\n%!" stake !sum_pp;
        ) stake_choices

    | "reward_diff" ->
      let num_samples = int_of_string Sys.argv.(2) in
      let epsilon = float_of_string Sys.argv.(3) in
      let stake_choices = List.init 13 (fun i -> 0.1 +. (float_of_int i) *. 0.03) in
      (* let stake_choices = [float_of_string Sys.argv.4)] in *)
      List.iter (fun stake ->
          let _fork_length, pp, total_proba, reward_percentage, _dr, _hr =
            expected_rewards ~params:default_params num_samples epsilon stake in
          Printf.printf "%.2f, %g, %g\n%!" stake pp reward_percentage;
          if 1. -. total_proba > precision then
            Printf.eprintf "Warning: total probability not 1 (but %g) for stake %.2f\n"
              total_proba stake;
        ) stake_choices

    | "seen" ->
      let params = default_params in

      let num_samples = int_of_string Sys.argv.(2) in
      let stake = float_of_string Sys.argv.(3) in
      let seen_blocks = int_of_string Sys.argv.(4) in
      let seen_delay = int_of_string Sys.argv.(5) in
      let depth = int_of_string Sys.argv.(6) in
      (* let res = explore stake depth in
       * Printf.printf "%.2f %.6f\n" stake res; *)

      assert(seen_delay mod params.delta_endo = 0);
      let seen_delay_ratio = seen_delay / params.delta_endo in
      let epsilon = 1e-9 in

      let _blks, wins, total = proba_seen_blocks ~params num_samples stake seen_blocks seen_delay_ratio epsilon in

      Printf.printf "\nAssuming that one has seen %i blocks baked with additional delay %i seconds with respect to the optimal %i minutes, the probability that an adversary that has fraction %.2f of the stake bakes a fork that does not include these blocks and is faster within the next %i blocks is about %g.\n"
        seen_blocks seen_delay seen_blocks stake depth wins;
      Printf.printf "We assume that the honest bakers bake as fast as they can in the future.\n";
      Printf.printf "(Estimate computed with %i samples. Total probability: %g -- should be 1 if the precision is satisfactory.)\n" num_samples total

    | "seen_blocks" ->
      seen_blocks_experiment ()

    | "seen_blocks_fork_length" ->
      let num_samples = int_of_string Sys.argv.(2) in
      let epsilon = float_of_string Sys.argv.(3) in
      let sb = int_of_string Sys.argv.(4) in
      let sdr = int_of_string Sys.argv.(5) in

      let stake_choices = List.init 39 (fun i -> float_of_int (10 + i) *. 0.01) in
      List.iter (fun stake ->
          let depth, proba_fork, total_proba = proba_seen_blocks_with_k_future_blocks
              ~params:default_params num_samples stake sb sdr (Epsilon epsilon) in
          if 1. -. total_proba > precision then
            Printf.eprintf "Warning: total probability not 1 (but %g) for stake %.2f\n"
              total_proba stake;
          Printf.printf "%.2f, %d\n%!" stake depth;
        )
        stake_choices

    (* same thing as above, but slower *)
    | "seen_blocks_fork_length_bis" ->
      let num_samples = int_of_string Sys.argv.(2) in
      let epsilon = float_of_string Sys.argv.(3) in
      let sb = int_of_string Sys.argv.(4) in
      let sdr = int_of_string Sys.argv.(5) in

      let stake_choices = List.init 36 (fun i -> float_of_int (10 + i) *. 0.01) in
      List.iter (fun stake ->
          let depth = ref 1 in
          let pp = ref (2. *. epsilon) in
          while !pp > epsilon do
            let ret_depth, proba_fork, total_proba = proba_seen_blocks_with_k_future_blocks
                ~params:default_params num_samples stake sb sdr (Depth !depth) in
            assert(ret_depth = !depth);
            if 1. -. total_proba > precision then
              Printf.eprintf "Warning: total probability not 1 (but %g) for stake %.2f and depth %d\n"
                total_proba stake !depth;
            pp := proba_fork;
            incr depth;
          done;
          Printf.printf "%.2f, %d\n%!" stake (!depth - 1);
        )
        stake_choices

    | "proba_as_function_of_odr" ->
      let num_samples = int_of_string Sys.argv.(2) in
      let epsilon = float_of_string Sys.argv.(3) in
      (* let stake_choices = [ 0.2; 0.3; 0.4; 0.45 ] in *)
      let stake_choices = List.init 36 (fun i -> float_of_int (10 + i) *. 0.01) in
      let sb_choices = [3; 5; 10] in
      Printf.printf "stake,n,od\n";

      let target_pp = 1e-8 in

      iter2 (fun stake sb ->
          let odr = ref 300 in
          let pp = ref (2. *. target_pp) in
          let total_pp = ref 0. in
          while !pp > target_pp do
            let _num_iter, proba_fork, total_proba = proba_seen_blocks
                ~params:default_params num_samples stake sb !odr epsilon in
            (* Printf.printf "  %.2f, %d, %d, %g\n%!" stake sb (8 * !odr) proba_fork; *)
            pp := proba_fork;
            total_pp := total_proba;
            decr odr
          done;
          Printf.printf "%.2f, %d, %d\n%!" stake sb (8 * !odr);
          if 1. -. !total_pp > 1e-6 then
            Printf.eprintf "Warning: total probability not 1 (but %g) for stake %.2f and odr %d\n"
              !total_pp stake !odr;
        )
        stake_choices sb_choices

    | "seen_proba_fork_length" ->
      let num_samples = int_of_string Sys.argv.(2) in
      let epsilon = float_of_string Sys.argv.(3) in
      let stake_choices = [ 0.2; 0.3; 0.4; 0.45 ] in
      let sb_choices = [3; 5] in
      let odr_choices = [0; 8; 16; 32] in
      Printf.printf "stake,n,od,depth,proba\n";
      iter3 (fun stake sb odr ->
          let depth = ref 1 in
          let pp = ref (2. *. epsilon) in
          while !pp > epsilon do
            let ret_depth, proba_fork, total_proba = proba_seen_blocks_with_k_future_blocks
                ~params:default_params num_samples stake sb odr (Depth !depth) in
            if 1. -. total_proba > 1e-6 then
              Printf.eprintf "Warning: total probability not 1 (but %g) for stake %.2f and depth %d\n"
                total_proba stake !depth;
            Printf.printf "%.2f, %d, %d,  %d, %g\n%!" stake sb (odr * default_params.delta_endo) !depth proba_fork;
            pp := proba_fork;
            incr depth
          done)
        stake_choices sb_choices odr_choices

    | "proba_as_function_of_n_seen_blocks" ->
      let num_samples = int_of_string Sys.argv.(2) in
      let precision = float_of_string Sys.argv.(3) in
      let target_proba = 1e-8 in
      let stake_choices = [ 0.2; 0.3; 0.4 ] in
      Printf.printf "stake,n,proba\n";
      let params = default_params in
      List.iter (fun stake ->
          let n = ref 1 in
          let pp = ref (2. *. target_proba) in
          while !pp > target_proba do
            let odr = 10 * !n in
            let _, proba_fork, _ = proba_seen_blocks ~params num_samples stake !n odr precision in
            Printf.printf "%.2f, %d, %d, %g\n%!" stake !n (odr * params.delta_endo) proba_fork;
            pp := proba_fork;
            incr n
          done)
        stake_choices

    | "seen_blocks_as_function_of_stake" ->
      let num_samples = int_of_string Sys.argv.(2) in
      let precision = float_of_string Sys.argv.(3) in
      let target_proba = 1e-8 in
      let stake_choices = List.init 25 (fun i -> float_of_int (20 + i) *. 0.01) in
      let odr_choices = [0; 16; 32] in
      Printf.printf "stake,n,od\n";
      let params = default_params in
      iter2 (fun stake odr ->
          let n = ref 1 in
          let pp = ref (2. *. target_proba) in
          while !pp > target_proba do
            let _, proba_fork, _ = proba_seen_blocks ~params num_samples stake !n odr precision in
            pp := proba_fork;
            incr n
          done;
          Printf.printf "%.2f, %d, %d\n%!" stake (!n-1) (odr * params.delta_endo);
        )
        stake_choices odr_choices

    | "gen_seen_blks_fork_length" ->
      let num_samples = int_of_string Sys.argv.(1) in
      let seen_blks_choices = [3; 5] in
      let seen_delays_ratio_choices = [(0, 0); (0, 8); (0, 16); (0, 32)] in
      let epsilon_choices = [10. ** (-2.); 10. ** (-3.); 10. ** (-4.)] in
      let stake_choices = List.init 39 (fun i -> float_of_int (10 + i) *. 0.01) in
      Printf.printf "stake,n,od,eps,fl\n%!";
      iter6 (fun stake ie dpde eps sb odr ->
          let depth, proba_fork, total_proba = proba_seen_blocks_with_k_future_blocks
              ~params:default_params num_samples stake sb odr (Epsilon eps) in
          if 1. -. total_proba > 1e-4 then
            Printf.eprintf "Warning: total probability not 1 (but %g) for stake %.2f\n"
              total_proba stake;
          Printf.printf "%.02f, %d, %d, %g, %d\n%!"
            stake sb (odr * default_params.delta_endo) eps depth;
        )
        stake_choices [24] [5.] epsilon_choices seen_blks_choices seen_delays_ratio_choices

    | command ->
      Printf.printf "Unknown command %s\n" command;
      print_string "Usage: forks <num_samples> fork <stake> <depth>
   or: forks <num_samples> seen <stake> <seen_blocks> <seen_delay> <depth>\n"


let test_proba_forks () =
  let params = default_params in
  let num_samples = int_of_string Sys.argv.(1) in
  let stake = float_of_string Sys.argv.(2) in

  let epsilons = [1e-3; 1e-6; 1e-9; 1e-12] in
  List.iter (fun epsilon ->
      Printf.printf "CASE epsilon = %g\n\n" epsilon;

      let sum = ref 0. in
      let prev_pp = ref 0. in
      let crt_pp = ref (2. *. epsilon) in
      let n = ref 1 in
      while !crt_pp > epsilon do
        prev_pp := !crt_pp;
        let pp, total = proba_fork_of_first_length ~params ~can_have_best_prio_at_first_block:true num_samples !n stake in
        Printf.printf "probability of a malicious fork of first length %d is %g (total = %g)\n" !n pp total;
        sum := !sum +. pp;
        crt_pp := pp;

        let pp, total = proba_fork_of_given_length ~params ~can_have_best_prio_at_first_block:true num_samples !n stake in
        Printf.printf "probability of a malicious fork of length %d is %g (total = %g)\n" !n pp total;

        incr n;
      done;
      print_newline();

      Printf.printf "for epsilon = %g, (using sum) probability of a \
                     malicious fork is %g\n" epsilon !sum;
      let n, pp, total = proba_fork
          ~params ~can_have_best_prio_at_first_block:true num_samples epsilon stake in
      Printf.printf "for epsilon = %g, probability of a malicious fork \
                     is %g (total = %g, nb_iter = %d)\n" epsilon pp total n;


      print_newline();
      print_newline();
    )
    epsilons


let test_proba_seen_blocks () =
  let params = default_params in
  let num_samples = int_of_string Sys.argv.(1) in
  let stake = float_of_string Sys.argv.(2) in
  let sb = int_of_string Sys.argv.(3) in
  let sdr = int_of_string Sys.argv.(4) in

  let epsilon = 1e-9 in

  let sum = ref 0. in
  let prev_pp = ref 0. in
  let crt_pp = ref (2. *. epsilon) in
  let k = ref 1 in
  while !crt_pp > epsilon do
    prev_pp := !crt_pp;
    let _depth, pp, total = proba_seen_blocks_with_k_future_blocks
        ~params num_samples stake sb sdr (Depth !k) in
    Printf.printf "probability of a malicious fork of first length %d is %g (total = %g)\n" !k pp total;
    sum := !sum +. pp;
    crt_pp := pp;

    incr k;
  done;
  print_newline();

  Printf.printf "(using sum) probability of a malicious fork is %g (nb_iter = %d)\n" !sum !k;
  let n, pp, total = proba_seen_blocks ~params num_samples stake sb sdr epsilon in
  Printf.printf "probability of a malicious fork is %g (total = %g, nb_iter = %d)\n" pp total n
