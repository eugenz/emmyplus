open Util


(* The dishonest baker D has a fraction [stake] of the stake. *)

(* 1 block: for any e1, an honest baker bakes a block B1 at time t1,
   with priority 0, and a dishonest baker D withholds his endorsements
   and bakes a block B'1 at time t'1, with priority 1, and B'1 arrives
   before B1: t1' <= t1.  D has e1 endorsements at level L. *)
let proba_1block ~params stake =
  (1. -. stake) *. (* D does not have priority 0 at level L *)
  stake *. (* D has priority 1 at level L *)
  (sum 0 params.total_endo (fun e1 ->
       (* D has e1 endorsements at level L *)
       let t1 = 60 + delay_endo ~params (params.typical_endo - e1) in
       let t1' = 60 + delay_prio ~params 1 in
       if t1' <= t1 then
         proba_endorsers ~params e1 stake
       else
         0.0 (* We are not in the "1 block" case, do not count it *)))

let proba_1block_with_rewards ~params ?(reward_policy = EmmyPlus) stake =
  let sum_pp = ref 0.0 in
  let steal_pp = ref 0.0 in
  let rewards_on_honest_chain = ref 0.0 in
  let rewards_on_dishonest_chain = ref 0.0 in
  let expected_gain = ref 0.0 in

  let baking_reward, endorsement_reward = reward_functions reward_policy in
  (* the relevant priority for level L+1 on the dishonest chain is:
     0 for Emmy (the priority of the block at level L) and
     1 for Emmy+ (the priority of the block at level L+1) *)
  let relevant_prio =
    match reward_policy with
    | Emmy -> 0
    | _ -> 1
  in
  for e1 = 0 to params.total_endo do
    (* D has e1 endorsements at level L *)
    let t1 = 60 + delay_endo ~params (params.typical_endo - e1) in
    let t1' = 60 + delay_prio ~params 1 in
    let pp =
      (1. -. stake) *. (* D does not have priority 0 at level L *)
      stake *. (* D has priority 1 at level L *)
      proba_endorsers ~params e1 stake (* D has e1 endorsements at level L *)
    in
    let reward_on_honest_chain = endorsement_reward 0 e1 in
    rewards_on_honest_chain := !rewards_on_honest_chain +. pp *. reward_on_honest_chain;
    let reward_on_dishonest_chain = baking_reward 1 params.typical_endo +. endorsement_reward relevant_prio e1 in
    if t1' <= t1 then
      begin
        steal_pp := !steal_pp +. pp;
        if reward_on_dishonest_chain >= reward_on_honest_chain then
          begin
            rewards_on_dishonest_chain := !rewards_on_dishonest_chain +. pp *. reward_on_dishonest_chain;
            expected_gain := !expected_gain +. pp *. (reward_on_dishonest_chain -. reward_on_honest_chain);
            sum_pp := !sum_pp +. pp
          end
      end
  done;
  !sum_pp, !steal_pp, !rewards_on_honest_chain, !rewards_on_dishonest_chain, !expected_gain

(* 2 blocks: for any k, e1, e2, an honest baker bakes a block B1 at
   level L and time t1, with priority 0, and a dishonest baker D
   withholds his endorsements and bakes a block B'1 at level L and
   time t'1, with priority 1, and we are not in scenario 1, that is,
   t1' > t1.

   Moreover, D has priorities 0 to k at the next level L+1, and does not
   have priority k+1 at level L+1.  So D bakes a block B'2 at level
   L+1 and time t2' with priority 0, an honest baker would bake a
   block B2 at level L+1 and time t2 with priority k+1, and B'2
   arrives before B2, that is, t2' <= t2.

   D has e1 endorsements at level L and e2 endorsements at level L+1.  *)
let proba_2blocks ~params stake =
  let kmax = roundup (prio_max_cst ~params) in
  (1. -. stake) *. (* D does not have priority 0 at level L *)
  stake *. (* D has priority 1 at level L *)
  (sum 0 params.total_endo (fun e1 ->
       let t1 = 60 + delay_endo ~params (params.typical_endo - e1) in
       let t1' = 60 + delay_prio ~params 1 in
       if t1' <= t1 then
         0.0 (* Already counted in "1 block" case, do not count it again *)
       else
         proba_endorsers ~params e1 stake *. (* D has e1 endorsements at level L *)
         ((* Value for k >= kmax
             When D has priorities 0...kmax at level L+1, we always have t2' <= t2
             independently of endorsements *)
           (pow (kmax + 1) stake) +.
           (* Value for k < kmax *)
           sum 0 (kmax - 1) (fun k ->
               (pow (k + 1) stake) *. (* D has priorities 0...k at level L+1 *)
               (1. -. stake) *. (* D does not have priority k+1 at level L+1 *)
               sum 0 params.total_endo (fun e2 ->
                   let t2 = t1 + 60 + delay_prio ~params (k + 1) +
                            delay_endo ~params (params.typical_endo - e2) in
                   let t2' = t1' + 60 + delay_endo ~params e2 in
                   if t2' <= t2 then
                     proba_endorsers ~params e2 stake (* D has e2 endorsements at level L+1 *)
                   else
                     0.0 (* We are not in the "2 blocks" case, do not count it *))
             ))))

let proba_2blocks_with_rewards ~params ?(reward_policy = EmmyPlus) stake =
  let sum_pp = ref 0.0 in
  let total_pp = ref 0.0 in
  let rewards_on_honest_chain = ref 0.0 in
  let rewards_on_dishonest_chain = ref 0.0 in
  let expected_gain = ref 0.0 in
  let baking_reward, endorsement_reward = reward_functions reward_policy in
  let relevant_prio_blk1, relevant_prio_blk2 =
    match reward_policy with
    | Emmy -> 0, 1
    | _ -> 1, 0
  in
  let kmax = roundup (prio_max_cst ~params) in
  for e1 = 0 to params.total_endo do
    let t1 = 60 + delay_endo ~params (params.typical_endo - e1) in
    let t1' = 60 + delay_prio ~params 1 in
    if t1 < t1' then
      (* else the attacker cannot get a bigger reward on his chain after two blocks;
         this was checked separately *)
      let reward_blk1_on_honest_chain = endorsement_reward 0 e1 in
      let reward_blk1_on_dishonest_chain = baking_reward 1 params.typical_endo +. endorsement_reward relevant_prio_blk1 e1 in
      let pp_prio1 =
        (1. -. stake) *. (* D does not have priority 0 at level L *)
        stake (* D has priority 1 at level L *)
      in
      (* D has e1 endorsements at level L *)
      let pp_endo_e1 = proba_endorsers ~params e1 stake in
      (* Value for k >= kmax
         When D has priorities 0...kmax at level L+1, we always have t2' <= t2
         independently of endorsements *)
      (* Value for k < kmax *)
      for e2 = 0 to params.total_endo do
        let reward_on_honest_chain =
          reward_blk1_on_honest_chain +.
          baking_reward 0 params.typical_endo +. endorsement_reward 0 e2 in
        let reward_on_dishonest_chain =
          reward_blk1_on_dishonest_chain +.
          baking_reward 0 e2 +. endorsement_reward relevant_prio_blk2 e2 in
        if reward_on_dishonest_chain > reward_on_honest_chain then
          let pp_endo_e2 = proba_endorsers ~params e2 stake in (* D has e2 endorsements at level L+1 *)
          (* case 1: k >= kmax; attacker steals the blocks and has profit *)
          let pp_prio2 =
            (pow (kmax + 1) stake)  (* D has priorities 0...kmax at level L+1 *)
          in
          let pp = pp_prio1 *. pp_endo_e1 *. pp_prio2  *. pp_endo_e2 in
          (* Printf.printf "%d %d %g\n" e1 e2 pp; *)
          sum_pp := !sum_pp +. pp;
          total_pp := !total_pp +. pp;
          expected_gain := !expected_gain +. pp *. (reward_on_dishonest_chain -. reward_on_honest_chain);
          (* case 2: k < kmax *)
          for k = 0 to kmax-1 do
            let pp_prio2 =
              (pow (k + 1) stake) *. (* D has priorities 0...k at level L+1 *)
              (1. -. stake) (* D does not have priority k+1 at level L+1 *)
            in
            let pp = pp_prio1 *. pp_endo_e1 *. pp_prio2  *. pp_endo_e2 in
            total_pp := !total_pp +. pp;
            let t2 = t1 + delay ~params (k + 1) (other_endo ~params e2) in
            let t2' = t1' + delay ~params 0 e2 in
            if t2' <= t2 then
              begin
                assert (t1' > t1);
                sum_pp := !sum_pp +. pp;
                expected_gain := !expected_gain +. pp *. (reward_on_dishonest_chain -. reward_on_honest_chain);
                rewards_on_honest_chain := !rewards_on_honest_chain +. pp *. reward_on_honest_chain;
                rewards_on_dishonest_chain := !rewards_on_dishonest_chain +. pp *. reward_on_dishonest_chain;
                (* Printf.printf "%d %d %d %g\n" e1 e2 k pp *)
              end
          done;
      done;
  done;
  !sum_pp, !total_pp, !rewards_on_honest_chain, !rewards_on_dishonest_chain, !expected_gain


(* 3 blocks: for any k1, k2, e1, e2, e3, an honest baker bakes a block
   B1 at level L and time t1, with priority 0, and a dishonest baker D
   withholds his endorsements and bakes a block B'1 at level L and
   time t'1, with priority 1, and we are not in scenario 1, that is,
   t1' > t1.

   Moreover, D has priorities 0 to k1 at the next level L+1, and does
   not have priority k1+1 at level L+1.  So D bakes a block B'2 at
   level L+1 and time t2' with priority 0, an honest baker bakes a
   block B2 at level L+1 and time t2 with priority k1+1, and we are
   not in scenario 2, that is, t2' > t2.

   At level L+2, D has priorities 0 to k2 but not k2+1.  D bakes a
   block B'3 at level L+2 at time t3' with priority 0, an honest baker
   bakes a block B3 at level L+2 and time t3 with priority k2+1, and
   t3' <= t3.

   D has e1 endorsements at level L, e2 endorsements at level L+1, e3
   at L+3. *)
let proba_3blocks ~params stake =
  let k1max = roundup (prio_max_cst ~params) in
  (1. -. stake) *. (* D does not have priority 0 at level L *)
  stake *. (* D has priority 1 at level L *)
  (sum 0 params.total_endo (fun e1 ->
       let t1 = 60 + delay_endo ~params (params.typical_endo - e1) in
       let t1' = 60 + delay_prio ~params 1 in
       if t1' <= t1 then
         0.0 (* Already counted in "1 block" case, do not count it again *)
       else
         (proba_endorsers ~params e1 stake) *. (* D has e1 endorsements at level L *)
         (* Value for k1 < k1max only; the case k1 >= k1max is entirely counted in case "2 blocks" *)
         sum 0 (k1max - 1) (fun k1 ->
             (pow (k1 + 1) stake) *. (* D has priorities 0...k1 at level L+1 *)
             (1. -. stake) *. (* D does not have priority k1+1 at level L+1 *)
             sum 0 params.total_endo (fun e2 ->
                 let t2 = t1 + 60 + delay_prio ~params (k1 + 1) + delay_endo ~params (params.typical_endo - e2) in
                 let t2' = t1' + 60 + delay_endo ~params e2 in
                 if t2' <= t2 then
                   0.0 (* Already counted in "2 blocks" case, do not count it again *)
                 else
                   (proba_endorsers ~params e2 stake) *. (* D has e2 endorsements at level L+1 *)
                   (let k2max = roundup (2. *. prio_max_cst ~params) - k1 - 1 in
                    (* Value for k2 >= k2max
                       When D has priorities 0...k2max at level L+2, we always have t3' <= t3 independently of endorsements *)
                    (pow (k2max + 1) stake) +.
                    (* Value for k2 < k2max *)
                    sum 0 (k2max - 1) (fun k2 ->
                        (pow (k2 + 1) stake) *. (* D has priorities 0...k2 at level L+2 *)
                        (1. -. stake) *. (* D does not have priority k2+1 at level L+2 *)
                        sum 0 params.total_endo (fun e3 ->
                            let t3 = t2 + 60 + delay_prio ~params (k2 + 1) + delay_endo ~params (params.typical_endo - e3) in
                            let t3' = t2' + 60 + delay_endo ~params e3 in
                            if t3' <= t3 then
                              proba_endorsers ~params e3 stake (* D has e3 endorsements at level L+2 *)
                            else
                              0.0 (* We are not in the "3 blocks" case, do not count it *)
                          )))))))



let scenario_to_string scenario =
  List.fold_right
    (fun (p,e) s -> s ^ " " ^ (string_of_int p) ^ "(" ^ (string_of_int e) ^ ")")
    scenario ""

(* [proba_blocks] generalizes the above functions to more blocks.
   H refers to the honest baker(s), D to the dishonest baker *)

let proba_blocks ~params stake max_depth print_info =
  (* if the current path (in the search tree) has a probability less
     than the [threshold], then we don't explore further that path *)
  let pp_threshold = 1e-10 in

  let pp_faster = ref 0.0 in
  let pp_profit = ref 0.0 in
  let pp_total = ref 0.0 in
  let expected_rewards = ref 0.0 in
  let expected_rewards' = ref 0.0 in
  let expected_gains = ref 0.0 in
  let gains_variance = ref 0.0 in
  let expected_gains_proportion = ref 0.0 in
  let gains_proportion_variance = ref 0.0 in
  let min_gain = ref 1.0 in
  let max_gain = ref 0.0 in

  (* [prio] encodes the minimal priorities at which the honest and the dishonest baker can bake;
     [actual_prios] returns:
     - the minimal priority at which H can bake
     - the minimal priority at which D can bake
     - the probability of this case to happen *)
  let actual_prios prio =
    if prio < 0 then
      0, -prio, (* H has priorities 0 to -prio-1, D has priority -prio *)
      (pow (-prio) (1. -. stake)) *. (* D does not have priorities 0 to -prio-1 *)
      stake (* D has priority -prio *)
    else
      prio, 0, (* H has priority prio, D has priorities 0 to prio-1  *)
      (pow prio stake) *. (* D has priorities 0 to prio-1 *)
      (1. -. stake) (* D does not have priority prio *)
  in
  let rec explore depth scenario pp t t' rewards rewards' =
    if depth > max_depth then
      pp_total := !pp_total +. pp
    else
      for e = 0 to params.total_endo do
        (* we explore all possible cases for the number [e] of
           endorsements D might have, but concerning priorities, we
           only explore the cases where the probability of the
           corresponding path is high enough *)
        let pp_endo = proba_endorsers ~params e stake in
        let rec explore_prios prio = (* [prio] is just an encoding, see [actual_prios] *)
          let continue = ref false in
          (* case 1, D has priority 0 *)
          if depth > 1 then
            (* for the first block, we exclude the case where D has prio 0 *)
            begin
              let prio_honest, prio_dishonest, pp_prio = actual_prios prio in
              let crt_pp = pp *. pp_prio *. pp_endo in
              let continue1 = crt_pp > pp_threshold in
              continue := !continue || continue1;
              compute prio prio_honest prio_dishonest e continue1
                depth scenario crt_pp t t' rewards rewards'
            end;

          (* case 2, D does not have priority 0 *)
          begin
            let prio = -prio in
            let prio_honest, prio_dishonest, pp_prio = actual_prios prio in
            let crt_pp = pp *. pp_prio *. pp_endo in
            let continue2 = crt_pp > pp_threshold in
            continue := !continue || continue2;
            compute prio prio_honest prio_dishonest e continue2
              depth scenario crt_pp t t' rewards rewards'
          end;

          if !continue then
            explore_prios (prio+1)
        in
        explore_prios 1
      done

  and compute prio prio_honest prio_dishonest e continue
      depth scenario crt_pp t t' rewards rewards' =
    let crt_scenario = (prio, e) :: scenario in
    if not continue && print_info then
      Printf.printf "stop at scenario %s\n" (scenario_to_string crt_scenario);
    let crt_t = t + delay ~params prio_honest (other_endo ~params e) in
    let endorsements_in_baked_block =
      if depth = 1 then params.typical_endo else e in
    let crt_t' = t' + delay ~params prio_dishonest endorsements_in_baked_block in
    let baking_reward, baking_reward' =
      if prio_honest > 0 then (* D bakes at zero priority on both chains *)
        baking_reward 0 params.typical_endo, baking_reward 0 endorsements_in_baked_block
      else (* D does not bake on honest chain, it bakes at non-zero priority on dishonest chain *)
        0., baking_reward prio_dishonest endorsements_in_baked_block
    in
    let crt_rewards = rewards +. baking_reward +. endorsement_reward 0 e in
    let crt_rewards' = rewards' +. baking_reward' +. endorsement_reward prio_dishonest e in
    (* [probas_catch_up] gives, for a given delay difference, the
       probability that the dishonest chain catches up the honest chain *)
    (* let pp_catch_up = proba_catch_up.(max_depth - depth).((crt_t' - crt_t) / params.delta_endo) in *)
    (* let continue = pp_catch_up >= pp_threshold in *)
    if crt_t' <= crt_t then
      begin
        (* dishonest chain has become faster *)
        pp_faster := !pp_faster +. crt_pp;
        if crt_rewards' >= crt_rewards then
          begin
            pp_total := !pp_total +. crt_pp;
            pp_profit := !pp_profit +. crt_pp;
            expected_rewards := !expected_rewards +. crt_rewards *. crt_pp;
            expected_rewards' := !expected_rewards' +. crt_rewards' *. crt_pp;
            let gain = crt_rewards' -. crt_rewards in
            if gain > !max_gain then max_gain := gain;
            if gain < !min_gain then min_gain := gain;
            expected_gains := !expected_gains +. gain *. crt_pp;
            gains_variance := !gains_variance +. gain *. gain *. crt_pp;
            let proportion = gain *. 100. /. crt_rewards in
            expected_gains_proportion := !expected_gains_proportion +. proportion *. crt_pp;
            gains_proportion_variance := !gains_proportion_variance +. proportion *. proportion *. crt_pp;
            (* D wins (he has a profit), no need to continue *)
            if print_info then
              Printf.printf "%g scenario = %s rew' = %.3f rew = %.3f diff = %d\n"
                crt_pp (scenario_to_string crt_scenario) crt_rewards' crt_rewards (crt_t - crt_t');
          end
        else if continue then
          explore (depth + 1) crt_scenario crt_pp crt_t crt_t' crt_rewards crt_rewards'
        else
          pp_total := !pp_total +. crt_pp
      end
    else if continue then
      explore (depth + 1) crt_scenario crt_pp crt_t crt_t' crt_rewards crt_rewards'
    else
      (* the probability of this path is very slow, we don't explore it further *)
      pp_total := !pp_total +. crt_pp
  in

  explore 1 [] 1. 0 0 0. 0.;

  let expected_gains = !expected_gains /. !pp_profit in
  let expected_gains_proportion = !expected_gains_proportion /. !pp_profit in

  !pp_faster, !pp_profit, !pp_total, !expected_rewards, !expected_rewards',
  !min_gain, !max_gain,
  expected_gains,
  !gains_variance /. !pp_profit -. expected_gains *. expected_gains,
  expected_gains_proportion,
  !gains_proportion_variance /. !pp_profit -. expected_gains_proportion *. expected_gains_proportion
