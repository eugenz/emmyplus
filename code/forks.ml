open Util

(* We consider the case when at a given level, an attacker wants to
   build a block at some level and he does not publish its
   block. Therefore an honest baker also builds a block at the same
   level, even if he does not have priority 0.

   This function computes the probability that the difference between
   the delay of attacker's block and the delay of honest bakers' block
   is [diff * delta_endo].

   Arguments:
   - [stake] is the attacker's fraction of the total stake
   - [first_block]: true iff the level is the first of a fork
       (Note that in this case the attacker has params.typical_endo
        endorsements, not just his own endorsements.)

   Let p the best priority of the honest baker and
   p' the best priority of the attacker.
   Let e be the number of endorsements of the honest baker and
   e' the number of endorsements of the attacker.

   NB: The difference is negative iff the attacker has an advantage.

   We let real_diff = diff * delta_endo.

   We have, by the definition of the block delay function:
     real_diff = delta_prio * (p' - p) + delta_endo * e_diff,
   where
     e_diff = max(0,24-e') - max(0,24-e)   if first_block = false
              0 - max(0,24-e))             otherwise
   Indeed, when we are at the first block of the fork, the attacker can
   include all endorsements (that is, more than init_endo).

   We obtain that
     p' - p = (delta_endo * diff  - delta_endo * e_diff) / delta_prio
   Therefore, we have
     p' - p = (diff - e_diff) / delta_ratio
   where delta_ratio = delta_prio / delta_endo.
   Note that we must have that the right side is an integer. Also p <> p',
   as the honest baker and the attacker cannot have the same best priority.
*)
let proba_diff ~params ?(can_have_best_prio_at_first_block = true)
    ~first_block ~stake diff =
  let sum = ref 0. in
  for e' = 0 to params.total_endo do
    let e = other_endo ~params e' in
    (* attacker has e' endorsements; the honest baker has e endorsements *)
    let e_diff = (if first_block then 0 else max 0 (params.init_endo - e')) -
                 max 0 (params.init_endo - e) in
    let prio_diff_aux = diff - e_diff in
    if prio_diff_aux mod delta_ratio ~params = 0 then
      begin
        let prio_diff = prio_diff_aux / delta_ratio ~params in
        (* note that p and p' need to be the minimal possible for the
           honest and the bad baker, respectively *)
        if prio_diff > 0 then
          (* the only possibility is that p = 0 and p' = prio_diff
             the honest baker also has the priorities 0,1,...,prio_diff-1 *)
          let prio_proba = pow prio_diff (1. -. stake) *. stake in
          let endo_proba = proba_endorsers ~params e' stake in
          sum := !sum +. prio_proba *. endo_proba
        else if prio_diff < 0 &&
                (not first_block || can_have_best_prio_at_first_block) then
          let prio_diff = - prio_diff in
          (* the only possibility is that p' = 0 and p = prio_diff
             the dishonest baker also has the priorities 0,1,...,prio_diff-1 *)
          let prio_proba = pow prio_diff stake *. (1. -. stake) in
          let endo_proba = proba_endorsers ~params e' stake in
          sum := !sum +. prio_proba *. endo_proba
      end;
  done;
  !sum

let proba_delay_wrt_max_speed ~params ~first_block ~stake diff =
  let sum = ref 0. in
  for e' = 0 to params.total_endo do
    (* attacker has e' endorsements *)
    let e_diff = (if first_block then 0 else max 0 (params.init_endo - e')) in
    let prio_diff_aux = diff - e_diff in
    let dr = delta_ratio ~params in
    if prio_diff_aux mod dr = 0 then
      let prio_diff = prio_diff_aux / dr in
      (* note that p and p' need to be the minimal possible for the
         honest and the bad baker, respectively *)
      if prio_diff >= 0 then
        (* the only possibility is that p' = prio_diff
           the honest baker also has the priorities 0,1,...,prio_diff-1 *)
        let prio_proba = pow prio_diff (1. -. stake) *. stake in
        let endo_proba = proba_endorsers ~params e' stake in
        sum := !sum +. prio_proba *. endo_proba
  done;
  !sum

(* [init_add_delay_display] takes as argument the number of samples,
   and returns three functions:
    - [proba_init f] initializes an array representing a probability
      distribution defined by the function [f]: [f diff] is the
      probability that the delay difference is [diff] * delta_endo.
    - [add_delay dist1_pp dist2_pp] is the probability distribution
      for the sum of two delays of probability distributions [dist1_pp]
      and [dist2_pp]
    - [display dist_pp] displays the probability distribution [dist_pp] *)

let init_add_delay_display num_samples =
  let diff_of_idx idx = idx - num_samples / 2 in
  let idx_of_diff d = num_samples / 2 + d in

  let add_delay crt_pp delta_pp =
    let next_pp = Array.make num_samples 0. in
    let sum_p = ref 0. in
    for idx = 0 to num_samples - 1 do
      let d = diff_of_idx idx in
      let sum = ref 0. in
      for d' = max (-num_samples/2) (d - num_samples/2)
        to min (num_samples/2) (d + num_samples/2)
      do
        let idx' = idx_of_diff d' in
        let idx'' = idx_of_diff (d - d') in
        sum := !sum +. crt_pp.(idx') *. delta_pp.(idx'')
      done;
      next_pp.(idx) <- !sum;
      sum_p := !sum_p +. !sum;
    done;
    next_pp, !sum_p
  in

  let proba_init f =
    Array.init num_samples (fun idx -> f (diff_of_idx idx))
  in

  let display crt_pp =
    let sum = ref 0. in
    for i = 0 to num_samples - 1 do
      sum := !sum +. crt_pp.(i);
    done;
  in

  (proba_init, add_delay, display)


let check_sum msg total_proba =
  let diff = 1. -. total_proba in
  if diff > 1e-10 then
    begin
      Printf.printf "%sthe sum of all probabilities is not 1: it is %g \
                     (diff = %g)\n" msg total_proba diff;
      assert false
    end

(* Probability that an attacker with stake fraction [stake] manages to
   build a chain of length [len] that is as fast as the honest chain,
   when [exclude_shorter_forks] is set to false.  When
   [exclude_shorter_forks] is true, then [len] is the smallest such
   value. That is, the function computes the probability that there is
   a fork of length [len] and no fork of shorter length.

   If [can_have_best_prio_at_first_block] is set to true, then we
   consider that the attacker may start a fork even if it has priority
   0 at the first fork level. *)
let proba_fork_length
    ~exclude_shorter_forks
    ~params
    ~can_have_best_prio_at_first_block
    num_samples
    len
    stake =

  assert (num_samples mod 2 = 1);
  assert (len >= 1);

  let proba_init, add_delay, _display = init_add_delay_display num_samples in

  let proba_diff_first_block = proba_diff
      ~params ~can_have_best_prio_at_first_block ~first_block:true ~stake in
  let proba_diff_next_blocks = proba_diff
      ~params ~can_have_best_prio_at_first_block ~first_block:false ~stake in

  let crt_pp = ref (proba_init proba_diff_first_block) in
  let one_step_pp = proba_init proba_diff_next_blocks in

  let sum_all_proba = ref 0. in
  let ignore_early_winning_proba () =
    for i = 0 to num_samples / 2 do
      sum_all_proba := !sum_all_proba +. (!crt_pp).(i);
      (!crt_pp).(i) <- 0.
    done
  in

  for i = 1 to len - 1 do
    if exclude_shorter_forks then
      ignore_early_winning_proba ();
    let next_pp, _ = add_delay (!crt_pp) one_step_pp in
    crt_pp := next_pp
  done;

  let proba_fork = ref 0. in
  for i = 0 to num_samples / 2 do
    proba_fork := !proba_fork +. !crt_pp.(i)
  done;

  sum_all_proba := !sum_all_proba +. !proba_fork;
  for i = 1 + num_samples / 2 to num_samples - 1 do
    sum_all_proba := !sum_all_proba +. !crt_pp.(i)
  done;

  !proba_fork, !sum_all_proba

(* The first function computes the probability that there is a fork of
   a given length and no fork of a shorter length. The second one
   allows for forks of shorter length. *)
let proba_fork_of_first_length = proba_fork_length ~exclude_shorter_forks:true
let proba_fork_of_given_length = proba_fork_length ~exclude_shorter_forks:false


(* Probability that an attacker with stake fraction [stake] manages to
   build a chain that is as fast as the honest chain. [epsilon]
   controls the precision of the computation: the probability is
   computed iteratively for increasing fork length; we stop the
   iteration when the difference between two consecutive probabilities
   is less than [epsilon]. *)
let proba_fork_epsilon ~params ~can_have_best_prio_at_first_block num_samples epsilon stake =
  assert (num_samples mod 2 = 1);

  let (proba_init, add_delay, _display) = init_add_delay_display num_samples in

  let proba_diff_first_block = proba_diff
      ~params ~can_have_best_prio_at_first_block ~first_block:true ~stake in
  let proba_diff_next_blocks = proba_diff
      ~params ~can_have_best_prio_at_first_block ~first_block:false ~stake in

  let crt_pp = ref (proba_init proba_diff_first_block) in
  let one_step_pp = proba_init proba_diff_next_blocks in

  let proba_faster = ref 0. in
  let add_winning_proba () =
    for i = 0 to num_samples / 2 do
      proba_faster := !proba_faster +. !crt_pp.(i);
      (!crt_pp).(i) <- 0.
    done
  in

  let fork_length = ref 0 in
  let prev_proba_faster = ref 0. in

  add_winning_proba();
  while !proba_faster -. !prev_proba_faster > epsilon *. !prev_proba_faster do
    let next_pp, __ = add_delay (!crt_pp) one_step_pp in
    crt_pp := next_pp;
    incr fork_length;
    (* As soon as the attacker is faster, we move the probability to
       the winning probability [proba_faster] and ignore the
       corresponding case in the rest of the computation. *)
    prev_proba_faster := !proba_faster;
    add_winning_proba();
  done;

  let sum_all_proba = ref !proba_faster in
  for i = 1 + num_samples / 2 to num_samples - 1 do
    sum_all_proba := !sum_all_proba +. !crt_pp.(i)
  done;

  !fork_length, !proba_faster, !sum_all_proba


(* Scenario:
   We know that the chain has [n_seen_blocks] blocks that have been
   baked in time [n_seen_blocks * 60 + seen_delay] seconds.
   During that time, an attacker baked his own chain with the stake
   he has (possibly doing double baking and/or double endorsements).
   After that, the attacker tries to catch up with the main chain.
   We output the probability that the attacker manages to have the
   fastest chain at some point.

   [seen_delay_ratio] is [seen_delay / delta_endo].
*)
let proba_seen_blocks ~params num_samples stake n_seen_blocks seen_delay_ratio
    epsilon =

  assert (num_samples mod 2 = 1);
  assert (seen_delay_ratio >= 0);
  assert (n_seen_blocks >= 1);
  assert (epsilon >= 0.);

  let proba_init, add_delay, display = init_add_delay_display num_samples in

  let shift crt_pp n =
    let next_pp = Array.make num_samples 0. in
    for idx = 0 to num_samples - 1 - n do
      next_pp.(idx) <- crt_pp.(idx + n)
    done;
    next_pp
  in
  let proba_delay_first_block =
    proba_delay_wrt_max_speed ~params ~first_block:true ~stake in
  let proba_delay_next_blocks =
    proba_delay_wrt_max_speed ~params ~first_block:false ~stake in
  let proba_diff_next_blocks =
    proba_diff ~params ~first_block:false ~stake in

  let delta_wrt_max_speed_pp = proba_init proba_delay_next_blocks in
  let delta_catchup_pp = proba_init proba_diff_next_blocks in

  (* The probability distribution after the 1st block of the fork *)
  let crt_pp = ref (proba_init proba_delay_first_block) in
  let msg = Printf.sprintf "[proba_seen_blocks] (past) at iteration 0, " in
  let total_p = Array.fold_left (+.) 0. !crt_pp in
  check_sum msg total_p;

  (* Update the probability after seeing blocks 2 to [n_seen_blocks] *)
  for i = 1 to n_seen_blocks - 1 do
    let pp, total_p = add_delay (!crt_pp) delta_wrt_max_speed_pp in
    let msg = Printf.sprintf "[proba_seen_blocks] (past) at iteration %d, " i in
    check_sum msg total_p;
    crt_pp := pp
  done;

  (* Adjust to take into account [seen_delay] *)
  crt_pp := shift (!crt_pp) seen_delay_ratio;
  let msg = Printf.sprintf "[proba_seen_blocks] (future) at iteration 0, " in
  let total_p = Array.fold_left (+.) 0. !crt_pp in
  check_sum msg total_p;

  let proba_catch_up_delay = ref 0. in
  let add_winning_proba() =
    for i = 0 to num_samples / 2 do
      proba_catch_up_delay := !proba_catch_up_delay +. !crt_pp.(i);
      (!crt_pp).(i) <- 0.
    done
  in

  let num_iter = ref 0 in
  let prev_proba_catch_up_delay = ref 0. in

  (* The attacker now tries to catch up *)
  add_winning_proba();
  while !proba_catch_up_delay -. !prev_proba_catch_up_delay >
        epsilon *. !prev_proba_catch_up_delay
  do
    incr num_iter;
    prev_proba_catch_up_delay := !proba_catch_up_delay;
    let pp, sum_p = add_delay !crt_pp delta_catchup_pp in
    crt_pp := pp;
    let total_p = !proba_catch_up_delay +. sum_p in
    let msg = Printf.sprintf "[proba_seen_blocks] (future) at iteration %d, " !num_iter in
    check_sum msg total_p;
    (* As soon as the attacker is faster, we move the probability
       to the winning probability [proba_catch_up_delay] and
       ignore the corresponding case in the rest of the computation. *)
    add_winning_proba();
  done;

  let sum = ref 0. in
  for i = 0 to num_samples - 1 do
    sum := !sum +. !crt_pp.(i)
  done;

  !num_iter, !proba_catch_up_delay, !sum +. !proba_catch_up_delay



let additional_reward ~params ~bake honest_blk_prio dishonest_blk_prio num_endo =
  let honest_blk_reward, dishonest_blk_reward =
    if bake then
      (* if the attacker bakes, then on the branches he bakes his
         block, that's why it the dishonest_blk_prio that is used *)
      baking_reward dishonest_blk_prio params.typical_endo,
      baking_reward dishonest_blk_prio num_endo
    else 0., 0.
  in
  let honest_endo_reward = endorsement_reward honest_blk_prio num_endo in
  let dishonest_endo_reward = endorsement_reward dishonest_blk_prio num_endo in
  let honest_reward = honest_blk_reward +. honest_endo_reward in
  let dishonest_reward = dishonest_blk_reward +. dishonest_endo_reward in
  let additional_reward = max 0. (dishonest_reward -. honest_reward) in
  additional_reward

let dh_reward ~params ~bake honest_blk_prio dishonest_blk_prio num_endo =
  let honest_blk_reward, dishonest_blk_reward =
    if bake then
      (* if the attacker bakes, then on the branches he bakes his
         block, that's why it the dishonest_blk_prio that is used *)
      baking_reward dishonest_blk_prio params.typical_endo,
      baking_reward dishonest_blk_prio num_endo
    else 0., 0.
  in
  let honest_endo_reward = endorsement_reward honest_blk_prio num_endo in
  let dishonest_endo_reward = endorsement_reward dishonest_blk_prio num_endo in
  let honest_reward = honest_blk_reward +. honest_endo_reward in
  let dishonest_reward = dishonest_blk_reward +. dishonest_endo_reward in
  (dishonest_reward, honest_reward)


(* note: [diff] represents the real difference in delays over [params.delta_endo] *)
let dh_expected_rewards ~params ~first_block ~stake diff =
  (* Printf.printf "[for_diff] %d (first_block = %b)\n" diff first_block; *)
  let sum_pp = ref 0. in
  let sum_dr = ref 0. in
  let sum_hr = ref 0. in
  for e' = 0 to params.total_endo do
    let e = other_endo ~params e' in
    (* attacker has e' endorsements; the honest baker has e endorsements *)
    let e_diff = (if first_block then 0
                  else max 0 (params.init_endo - e')) - max 0 (params.init_endo - e) in
    let prio_diff_aux = diff - e_diff in
    if prio_diff_aux mod delta_ratio ~params = 0 then
      begin
        let prio_diff = prio_diff_aux / delta_ratio ~params in
        (* note that p and p' need to be the minimal possible for the
           honest and the bad baker, respectively *)
        if prio_diff > 0 then
          (* the only possibility is that p = 0 and p' = prio_diff
             the honest baker also has the priorities 0,1,...,prio_diff-1 *)
          let prio_proba = pow prio_diff (1. -. stake) *. stake in
          let endo_proba = proba_endorsers ~params e' stake in
          let pp = prio_proba *. endo_proba in
          sum_pp := !sum_pp +. pp;
          let (dr, hr)  = dh_reward ~params ~bake:false 0 prio_diff e' in
          sum_dr := !sum_dr +. pp *. dr;
          sum_hr := !sum_hr +. pp *. hr;
        else if prio_diff < 0 && not first_block then
          let prio_diff = - prio_diff in
          (* the only possibility is that p' = 0 and p = prio_diff
             the dishonest baker also has the priorities 0,1,...,prio_diff-1 *)
          let prio_proba = pow prio_diff stake *. (1. -. stake) in
          let endo_proba = proba_endorsers ~params e' stake in
          let pp = prio_proba *. endo_proba in
          sum_pp := !sum_pp +. pp;
          let (dr, hr)  = dh_reward ~params ~bake:true prio_diff 0 e' in
          sum_dr := !sum_dr +. pp *. dr;
          sum_hr := !sum_hr +. pp *. hr;
      end;
  done;
  !sum_pp, !sum_dr, !sum_hr


let rewards_init ~params num_samples =
  let diff_of_idx idx = idx - num_samples / 2 in
  let idx_of_diff d = num_samples / 2 + d in

  let add_delay crt delta =
    let next = Array.make num_samples (0., 0., 0.) in
    for idx = 0 to num_samples - 1 do
      let d = diff_of_idx idx in
      let sum_pp = ref 0. in
      let sum_drewards = ref 0. in
      let sum_hrewards = ref 0. in
      for d' = max (-num_samples/2) (d - num_samples/2)
        to min (num_samples/2) (d + num_samples/2)
      do
        let idx' = idx_of_diff d' in
        let idx'' = idx_of_diff (d - d') in
        let crt_pp, crt_drewards, crt_hrewards = crt.(idx') in
        let delta_pp, delta_drewards, delta_hrewards = delta.(idx'') in
        sum_pp := !sum_pp +. crt_pp *. delta_pp;
        sum_drewards := !sum_drewards +. crt_drewards *. delta_pp +. delta_drewards *. crt_pp;
        sum_hrewards := !sum_hrewards +. crt_hrewards *. delta_pp +. delta_hrewards *. crt_pp;
      done;
      next.(idx) <- (!sum_pp, !sum_drewards, !sum_hrewards)
    done;
    next
  in

  let init_array f =
    Array.init num_samples (fun idx -> f (diff_of_idx idx))
  in

  let display crt =
    let sum_pp = ref 0. in
    let sum_drewards = ref 0. in
    let sum_hrewards = ref 0. in
    for i = 0 to num_samples - 1 do
      let (pp, dr, hr) = crt.(i) in
      sum_pp := !sum_pp +. pp;
      sum_drewards := !sum_drewards +. dr *. pp;
      sum_hrewards := !sum_hrewards +. hr *. pp;
      Printf.printf "%i %g %g %g\n" (diff_of_idx i) pp dr hr;
    done;
    Printf.printf "sum_pp = %g sum_drewards = %f sum_hrewards = %f\n" !sum_pp !sum_drewards !sum_hrewards;
  in

  (init_array, add_delay, display)


let expected_rewards ~params num_samples epsilon stake =
  assert (num_samples mod 2 = 1);

  let (init_rewards_array, add_delay, display) = rewards_init ~params num_samples in

  let crt = ref (init_rewards_array (dh_expected_rewards ~params ~first_block:true ~stake)) in
  let one_step = init_rewards_array (dh_expected_rewards ~params ~first_block:false ~stake) in

  (* Printf.printf "for first block:\n";
   * display !crt;
   * Printf.printf "for each new block:\n";
   * display one_step; *)

  let proba_faster = ref 0. in
  let drewards = ref 0. in
  let hrewards = ref 0. in
  let add_winning_proba () =
    for i = 0 to num_samples / 2 do
      let (pp, dr, hr) = (!crt).(i) in
      proba_faster := !proba_faster +. pp;
      drewards := !drewards +. dr *. pp;
      hrewards := !hrewards +. hr *. pp;
      (!crt).(i) <- (0., 0., 0.)
    done
  in

  let fork_length = ref 1 in
  let prev_proba_faster = ref 0. in

  add_winning_proba();
  while !proba_faster -. !prev_proba_faster > epsilon *. !prev_proba_faster do
    crt := add_delay !crt one_step;
    incr fork_length;
    (* Printf.printf "for block %d:\n%!" !fork_length;
     * display !crt; *)
    (* As soon as the attacker is faster, we move the probability to
       the winning probability [proba_faster] and ignore the
       corresponding case in the rest of the computation. *)
    prev_proba_faster := !proba_faster;
    add_winning_proba();
  done;

  let rest_pp = ref 0. in
  for i = 1 + num_samples / 2 to num_samples - 1 do
    let (fst, dr, hr) = (!crt).(i) in
    rest_pp := !rest_pp +. fst
  done;

  let total_proba = !rest_pp +. !proba_faster in

  let reward_percentage = 100. *. !drewards /. !hrewards in

  !fork_length, !proba_faster, total_proba, reward_percentage, !drewards, !hrewards
