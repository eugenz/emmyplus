(* [pow k x] is [x^k] *)
let rec pow k x =
  if k = 0 then 1. else (pow (k-1) x) *. x

let rec fact n =
  if n = 0 then 1. else (float_of_int n) *. fact (n-1)

let binom_coef k n =
  (fact n) /. ((fact k) *. (fact (n-k)))


type parameters = {
  total_endo : int;
  typical_endo : int;
  init_endo : int;
  delta_prio : int;
  delta_endo : int;
}


let delay_endo ~params e =
  params.delta_endo * (max 0 (params.init_endo - e))

let delay_prio ~params prio =
  params.delta_prio * prio

let delay ~params p e =
  60 + delay_prio ~params p + delay_endo ~params e

let other_endo ~params e =
  max 0 (params.typical_endo - e)

let delta_ratio ~params =
  if params.delta_prio mod params.delta_endo = 0 then
    params.delta_prio / params.delta_endo
  else
    failwith "delta_prio is not a multiple of delta_endo"


(* [proba_endorsers ~params n_end stake] is the probability that a delegate
   with proportion [stake] of the stake has [n_end] endorsers in a block,
   knowing that the total number of endorsers in a block is [params.total_endo] *)
let proba_endorsers ~params n_end stake =
  if (n_end < 0) || (n_end > params.total_endo) then 0. else
    (binom_coef n_end params.total_endo) *.
    (stake ** (float_of_int n_end)) *.
    ((1. -. stake) ** (float_of_int (params.total_endo - n_end)))


let emmy_baking_reward _prio _num_endo = 16.0

let baking_reward prio num_endo =
  16.0 /. (1. +. float_of_int prio) *. (0.8 +. 0.2 *. (float_of_int num_endo) /. 32.)

let baking_reward_bug prio num_endo =
  16.0 /. (1. +. float_of_int prio) *. (float_of_int (8 + 2 * num_endo / 32)) /. 10.


let endorsement_reward prio num_endo =
  (float_of_int num_endo) *. 2.0 /. (1. +. float_of_int prio)

(* same as the one for Emmy+, it's just that here [prio] has a different meaning *)
let emmy_endorsement_reward prio num_endo =
  endorsement_reward prio num_endo

let baking_reward_non_linear prio num_endo =
  let e = float_of_int num_endo in
  16. /. (1. +. float_of_int prio) *. e *. e /. 1024.

let baking_reward_non_linear_var prio num_endo =
  let e = float_of_int num_endo in
  16. /. (1. +. float_of_int prio) *. (0.8 +. 0.2 *. e *. e /. 1024.)

let baking_reward_simple prio num_endo =
  1.25 /. (1. +. float_of_int prio) *. (float_of_int num_endo)

let endorsement_reward_simple prio num_endo =
  1.25 /. (1. +. float_of_int prio) *. (float_of_int num_endo)

let baking_reward_13_18 prio num_endo =
  1. /. (1. +. float_of_int prio) *. (float_of_int num_endo) *. 13. /. 18.

let endorsement_reward_13_18 prio num_endo =
  1. /. (1. +. float_of_int prio) *. (float_of_int num_endo) *. 32. /. 18.


type reward_policy =
  | Emmy | EmmyPlus | EmmyPlusBug
  | Nonlinear
  | Nonlinear_var
  | Simple
  | Policy_13_18

let reward_functions reward_policy =
  match reward_policy with
  | Emmy -> emmy_baking_reward, emmy_endorsement_reward
  | EmmyPlus -> baking_reward, endorsement_reward
  | EmmyPlusBug -> baking_reward_bug, endorsement_reward
  | Nonlinear -> baking_reward_non_linear, endorsement_reward
  | Nonlinear_var -> baking_reward_non_linear_var, endorsement_reward
  | Simple -> baking_reward_simple, endorsement_reward_simple
  | Policy_13_18 -> baking_reward_13_18, endorsement_reward_13_18


let prio_max_cst ~params =
  (float_of_int params.init_endo) *.
  (float_of_int params.delta_endo) /.
  (float_of_int params.delta_prio)


let curve f =
  let rec points x =
    if x <= 0.5 then
      begin
        print_string "("; print_float x; print_string ", ";
        print_float (f x); print_string ") --\n";
        if x <= 0.49 then
          points (x +. 0.01)
        else
          points (x +. 0.001)
      end
    else
      begin
        print_string "("; print_float x; print_string ", ";
        print_float (f x); print_string ");"
      end
  in
  points 0.

let curve_log f = curve (fun x -> log10 (f x))




(* [sum min max f] is [f(min) + f(min+1) + ... + f(max)] *)
let sum min max f =
  let rec aux accu min =
    if max < min then accu else
      aux (accu +. f(min)) (min+1)
  in
  aux 0. min

let roundup x =
  truncate (x +. 0.99999999)

let iter2 f l1 l2 =
  List.iter (fun x1 ->
      List.iter (fun x2 ->
          f x1 x2)
        l2)
    l1

let iter2_1 f l1 l2 =
  List.iter (fun x1 ->
      (List.iter (fun (dp_coeff, de_coeff) ->
          let r = dp_coeff * 40 + de_coeff in
               f x1 r)
          l2))
    l1

let iter3 f l1 l2 l3 =
  List.iter (fun x1 ->
      (List.iter (fun x2 ->
           (List.iter (fun x3 ->
                f x1 x2 x3)
               l3))
          l2))
    l1

let iter3_1 f l1 l2 l3 =
  List.iter (fun x1 ->
      (List.iter (fun x2 ->
          (List.iter (fun (dp_coeff, de_coeff) ->
               let r = dp_coeff * 40 + de_coeff in
               f x1 x2 r)
              l3))
        l2))
    l1

let iter4 f l1 l2 l3 l4 =
  List.iter (fun x1 ->
      (List.iter (fun x2 ->
           (List.iter (fun x3 ->
                (List.iter (fun x4 ->
                     f x1 x2 x3 x4 )
                    l4))
               l3))
          l2))
    l1

let iter6 f l1 l2 l3 l4 l5 l6 =
  List.iter (fun x1 ->
      (List.iter (fun dp ->
           (List.iter (fun x3 ->
                (List.iter (fun x4 ->
                     (List.iter (fun x5 ->
                          (List.iter (fun x6 ->
                               f x1 dp x3 x4 x5 x6)
                              l6))
                         l5))
                    l4))
               l3))
          l2))
    l1

let iter6_1 f l1 l2 l3 l4 l5 l6 =
  List.iter (fun x1 ->
      (List.iter (fun dp ->
           (List.iter (fun x3 ->
                (List.iter (fun x4 ->
                     (List.iter (fun x5 ->
                          (List.iter (fun (dp_coeff, de_coeff) ->
                               let r = dp_coeff * dp + de_coeff in
                               f x1 dp x3 x4 x5 r)
                              l6))
                         l5))
                    l4))
               l3))
          l2))
    l1
