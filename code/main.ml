open Util
open Simulations
open Selfish_baking
open Forks

let precision = 1e-6

let default_params = {
   total_endo = 32;
   typical_endo = 31;
   init_endo = 24;
   delta_prio = 40;
   delta_endo = 8;
}

let main () =
  if Array.length Sys.argv < 2 then
    print_string "Usage: see possible commands options in main.ml\n"
  else
    match Sys.argv.(1) with

    | "past_fork_rates_depending_on_stake" ->
      let num_samples = int_of_string Sys.argv.(2) in
      let seen_blks_choices = [3; 6] in
      let od_choices = [0; 128; 256] in
      let precision_choices = [10. ** (-12.)] in
      let init_endo_choices = [24] in
      let dpde_choices = [5] in
      let stake_choices = List.init 30 (fun i -> float_of_int (20 + i) *. 0.01) in
      Printf.printf "stake,n,od,proba,error\n%!";
      iter6 (fun ie dpde precision stake n od ->
          let _, pp, tot = proba_seen_blocks
              ~params: { default_params with init_endo = ie; delta_prio = dpde; delta_endo = 1}
              num_samples stake n (od / default_params.delta_endo) precision in
          Printf.printf "%.02f, %d, %d, %g, %g\n%!"
            stake n od pp (1. -. tot)
        )
        init_endo_choices dpde_choices precision_choices stake_choices seen_blks_choices od_choices

    | "past_fork_rates_depending_on_fork_length" ->
      let num_samples = int_of_string Sys.argv.(2) in
      let precision = float_of_string Sys.argv.(3) in
      let target_proba = 1e-8 in
      let stake_choices = [ 0.2; 0.3; 0.4 ] in
      Printf.printf "stake,n,od,proba\n";
      let params = default_params in
      List.iter (fun stake ->
          let n = ref 1 in
          let pp = ref (2. *. target_proba) in
          while !pp > target_proba do
            let odr = 8 * !n in
            let _, proba_fork, _ = proba_seen_blocks ~params num_samples stake !n odr precision in
            Printf.printf "%.2f, %d, %d, %g\n%!" stake !n (odr * params.delta_endo) proba_fork;
            pp := proba_fork;
            incr n
          done)
        stake_choices

    | "now_fork_rates_depending_on_fork_length" ->
      let num_samples = int_of_string Sys.argv.(2) in
      (* let stake_choices = List.init 20 (fun i -> float_of_int (20 + i) *. 0.01) in *)
      let stake_choices = [ 0.2; 0.3; 0.4 ] in
      let target_proba = 1e-8 in
      Printf.printf "stake,n,proba\n";
      List.iter (fun stake ->
          let depth = ref 1 in
          let pp = ref (2. *. target_proba) in
          while !pp > target_proba do
            let proba_fork, total_proba = proba_fork_of_given_length ~params:default_params
                ~can_have_best_prio_at_first_block:true num_samples !depth stake in
            if 1. -. total_proba > precision then
              Printf.eprintf "Warning: total probability not 1 (but %g) for stake %.2f and depth %d\n"
                total_proba stake !depth;
            pp := proba_fork;
            Printf.printf "%.2f, %d, %g\n%!" stake !depth proba_fork;
            incr depth;
          done;
        )
        stake_choices


    | "selfish_baking_scenarios" -> failwith "see main_all"

    | "selfish_baking_general" ->
      let num_samples = int_of_string Sys.argv.(2) in
      let epsilon = float_of_string Sys.argv.(3) in
      let stake_choices = List.init 31 (fun i -> float_of_int (20 + i) *. 0.01) in
      List.iter (fun stake ->
          let _nb_iter, pp, _pp_total = proba_fork_epsilon ~can_have_best_prio_at_first_block:false
              ~params:default_params
              num_samples epsilon stake in
          Printf.printf "%.2f, %g\n%!" stake pp;
        ) stake_choices

    | "selfish_baking_rewards_scenarios" ->
      let params = default_params in
      Printf.printf "stake,\
                     abs_erew_1_emmy+,rel_erew_1_emmy+,abs_erew_2_emmy+,rel_erew_2_emmy+,
                     sum_abs_emmy+,sum_rel_emmy+,\
                     abs_erew_1_emmy,rel_erew_1_emmy,abs_erew_2_emmy,rel_erew_2_emmy,\
                     sum_abs_emmy,sum_rel_emmy\n";
      List.iter (fun stake ->
          let _, pp_t1, rew_hc1, rew_dc1, _ = proba_1block_with_rewards ~params ~reward_policy:EmmyPlus stake in
          let _, pp_t2, rew_hc2, rew_dc2, _ = proba_2blocks_with_rewards ~params ~reward_policy:EmmyPlus stake in

          let _, pp_t1e, rew_hc1e, rew_dc1e, _ = proba_1block_with_rewards ~params ~reward_policy:Emmy stake in
          let _, pp_t2e, rew_hc2e, rew_dc2e, _ = proba_2blocks_with_rewards ~params ~reward_policy:Emmy stake in

          Printf.printf "%.2f, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g\n"
            stake
            rew_dc1
            (rew_dc1 /. pp_t1)
            rew_dc2
            (rew_dc2 /. pp_t2)
            (rew_dc1 +. rew_dc2)
            (rew_dc1 /. pp_t1 +. rew_dc2 /. pp_t2)
            rew_dc1e
            (rew_dc1e /. pp_t1e)
            rew_dc2e
            (rew_dc2e /. pp_t2e)
            (rew_dc1e +. rew_dc2e)
            (rew_dc1e /. pp_t1e +. rew_dc2e /. pp_t2e)
        )
        (List.init 50 (fun i -> 0.1 +. (float_of_int i) /. 100.))

    | "selfish_baking_rewards_percentage" ->
      let num_samples = int_of_string Sys.argv.(2) in
      let epsilon = float_of_string Sys.argv.(3) in
      let stake_choices = List.init 40 (fun i -> 0.02 +. (float_of_int i) *. 0.001) @
                          List.init 42 (fun i -> 0.06 +. (float_of_int i) *. 0.01) in
      Printf.printf "stake,proba,erew_diff\n";
      List.iter (fun stake ->
          let _fork_length, pp, total_proba, reward_percentage, _dr, _hr =
            expected_rewards ~params:default_params num_samples epsilon stake in
          Printf.printf "%f, %g, %g\n%!" stake pp reward_percentage;
          if 1. -. total_proba > precision then
            Printf.eprintf "Warning: total probability not 1 (but %g) for stake %.2f\n"
              total_proba stake;
        ) stake_choices

    | "rates_cts" ->
      let num_samples = int_of_string Sys.argv.(2) in
      let seen_blks_choices = [3; 5] in
      let od_choices = [0; 128; 256] in
      let precision_choices = [10. ** (-12.)] in
      let init_endo_choices = [16; 18; 20; 22; 24; 28; 30] in
      let dpde_choices = List.init 20 (fun x -> 1 + x) in
      let stake_choices = [0.2; 0.25; 0.3; 0.35; 0.4; 0.45] in
      Printf.printf "stake,n,od,ie,dpde,proba,error\n%!";
      iter6 (fun ie dpde precision stake n od ->
          let _, pp, tot = proba_seen_blocks
              ~params: { default_params with init_endo = ie; delta_prio = dpde; delta_endo = 1}
              num_samples stake n (od / default_params.delta_endo) precision in
          Printf.printf "%.02f, %d, %d, %d, %d, %g, %g\n%!"
            stake n od ie dpde pp (1. -. tot)
        )
        init_endo_choices dpde_choices precision_choices stake_choices seen_blks_choices od_choices

    | "steal_block_cts" ->
      let num_samples = int_of_string Sys.argv.(2) in
      let epsilon = float_of_string Sys.argv.(3) in
      let init_endo_choices = [16; 18; 20; 22; 24; 28; 30] in
      let dpde_choices = List.init 20 (fun x -> 1 + x) in
      let stake_choices = [0.2; 0.25; 0.3; 0.35; 0.4; 0.45] in
      Printf.printf "init_endo,dpde,stake,proba\n%!";
      iter3 (fun ie dpde f ->
          let _nb_iter, pp, _pp_total = proba_fork_epsilon ~can_have_best_prio_at_first_block:false
              ~params: { default_params with init_endo = ie; delta_prio = dpde; delta_endo = 1}
              num_samples epsilon f in
          Printf.printf "%d, %d, %.02f, %g\n%!"
            ie dpde f pp;
        )
        init_endo_choices dpde_choices stake_choices

    | "scenario_rewards_csv" ->
      let params = default_params in
      let f_proba = match Sys.argv.(2) with
        | "(1)" -> proba_1block_with_rewards ~params ~reward_policy:EmmyPlus
        | "(2)" -> proba_2blocks_with_rewards ~params ~reward_policy:EmmyPlus
        | "(1emmy)" -> proba_1block_with_rewards ~params ~reward_policy:Emmy
        | "(2emmy)" -> proba_2blocks_with_rewards ~params ~reward_policy:Emmy
        | _ -> failwith "unknown scenario: use '(1)' or '(2)' or '(1emmy)' or '(2emmy)'"
      in
      List.iter (fun stake ->
          let pp, pp_total, reward_honest_chain, reward_dishonest_chain, _expected_gain =
            f_proba stake in
          Printf.printf "%.2f, %g, %g, %g, %g\n"
            stake pp pp_total reward_honest_chain reward_dishonest_chain)
        (List.init 50 (fun i -> 0.1 +. (float_of_int i) /. 100.))

    | "selfish_baking_all" ->
      let stake = float_of_string Sys.argv.(2) in
      let max_depth = int_of_string Sys.argv.(3) in
      let print_info = bool_of_string Sys.argv.(4) in
      let pp_faster, pp_profit, pp_total,
          expected_rewards, expected_rewards', min_gain, max_gain, expected_gains,
          gains_variance,
          expected_gains_proportion,
          gains_proportion_variance
        =
        proba_blocks ~params:default_params stake max_depth print_info in
      Printf.printf "pp_faster = %g, pp_profit = %g, pp_total = %g, expected_rewards = %g, expected_rewards = %g\n"
        pp_faster pp_profit pp_total expected_rewards expected_rewards

    | "rewards_scenario" ->
      let params = default_params in
      let f_proba = match Sys.argv.(2) with
        | "(1)" -> proba_1block_with_rewards ~params ~reward_policy:EmmyPlus
        | "(2)" -> proba_2blocks_with_rewards ~params ~reward_policy:EmmyPlus
        | "(1s)" -> proba_1block_with_rewards ~params ~reward_policy:Simple
        | "(2s)" -> proba_2blocks_with_rewards ~params ~reward_policy:Simple
        | "(1emmy)" -> proba_1block_with_rewards ~params ~reward_policy:Emmy
        | "(2emmy)" -> proba_2blocks_with_rewards ~params ~reward_policy:Emmy
        | "(1bug)" -> proba_1block_with_rewards ~params ~reward_policy:EmmyPlusBug
        | "(2bug)" -> proba_2blocks_with_rewards ~params ~reward_policy:EmmyPlusBug
        | "(1nl)" -> proba_1block_with_rewards ~params ~reward_policy:Nonlinear
        | "(2nl)" -> proba_2blocks_with_rewards ~params ~reward_policy:Nonlinear
        | "(1nlvar)" -> proba_1block_with_rewards ~params ~reward_policy:Nonlinear_var
        | "(2nlvar)" -> proba_2blocks_with_rewards ~params ~reward_policy:Nonlinear_var
        | "(1_13/18)" -> proba_1block_with_rewards ~params ~reward_policy:Policy_13_18
        | "(2_13/18)" -> proba_2blocks_with_rewards ~params ~reward_policy:Policy_13_18
        | _ -> failwith "unknown scenario"
      in
      (* let one_year_in_minutes = float_of_int (365 * 24 * 60) in *)
      Printf.printf "stake,proba_steal,proba_gain,rewards,percentage\n";
      List.iter (fun stake ->
          let pp, steal_pp, reward_honest_chain, reward_dishonest_chain, expected_gain =
            f_proba stake in
          Printf.printf "%.2f, %g, %g, %g, %.6f\n"
            stake steal_pp pp
            (* reward_honest_chain reward_dishonest_chain *)
            expected_gain
            (100. *. expected_gain /. (stake *. 80.))
        )
        (List.init 1 (fun i -> 0.3 +. (float_of_int i) /. 20.))

    | command ->
      Printf.printf "Unknown command %s (see main.ml)\n" command
   (*    print_string "Usage: analysis <num_samples> fork <stake> <depth>
    * or: analysis <num_samples> seen <stake> <seen_blocks> <seen_delay> <depth>\n" *)

let _ =
    main ()
