#!/bin/bash

DATA_DIR=.
BIN=../code/analysis
TMP="$(mktemp -d -t tmp-XXX -p .)"

test_case() {
	case=$1
	shift
	OUT="$TMP/$case.csv"
	echo "Executing: $BIN $case $@ > $OUT"
	time $BIN $case $@ > $OUT
	diff -q "$DATA_DIR/$case.csv" $OUT
	if [ $? -eq 0 ]; then
        echo "Same results for case $case."
        rm $OUT
    fi
}

# https://stackoverflow.com/questions/1494178/how-to-define-hash-tables-in-bash
declare -A cases=(
	# ["past_fork_rates_depending_on_stake"]="2001 10^-12"            # 30min
	# ["past_fork_rates_depending_on_fork_length"]="5001 1e-12"       # 4min
	# ["now_fork_rates_depending_on_fork_length"]="2001"              # 22sec
	# ["selfish_baking_scenarios"]=""                                 # FAILURE: unclear scenario!
	# ["selfish_baking_rewards_percentage"]="3001 1e-10"              # 2m55sec
	# ["selfish_baking_rewards_scenarios"]=""                         # 1sec, REAL DIFFS
	# ["rates_cts"]="2001"                                            # 120min!
	# ["steal_block_cts"]="2001 1e-12"                                # 16min
)

echo "Outputting the results in the directory $TMP"

for c in "${!cases[@]}"; do
	test_case $c ${cases[$c]}
done

if [ ! "$(ls -A $TMP)" ]; then
	echo "All results are the same as the stored ones; removing $TMP"
    rmdir "$TMP"
fi
